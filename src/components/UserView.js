import React, { useState, useEffect } from "react";
import ProductCard from "./ProductCard";

export default function UserView({ productData }) {
    //nasa curly braces kasi un[packed]
    const [product, setProduct] = useState();

    useEffect(() => {
        const productArr = productData.map((product) => {
            //only active courses
            if (product.isActive === true) {
                return (
                    <div
                        className="col-md-6 col-lg-4 col-xl-4"
                        key={product._id}
                        id="card"
                    >
                        <ProductCard productProp={product}></ProductCard>
                    </div>
                );
            } else {
                return null;
            }
        });
        setProduct(productArr);
    }, [productData]);

    return (
        <>
            <section className="section-products">
                <div className="container">
                    <div className="row justify-content-center text-center">
                        <div className="col-md-8 col-lg-6">
                            <div className="header">
                                <h3>
                                    <em>second quarter sale</em>
                                </h3>
                                <h2>this month's best selling books:</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">{product}</div>
            </section>
        </>
    );
}
