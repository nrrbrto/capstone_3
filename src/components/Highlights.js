import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import pic1 from './1.jpg';
import pic2 from './2.jpg';
import pic3 from './3.jpg';

export default function Highlights() {
    return (
        <div>

            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active" id="red"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1" id="red"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2" id="red"></li>
                </ol>
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img className="d-block w-100" src={pic1} alt="First slide" />
                        <div className="carousel-caption">
                            <h1><i>home.</i></h1>
                            <p id="red">you are at the start of the page. feel free to look around</p>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img className="d-block w-100" src={pic2} alt="Second slide" />
                        <div className="carousel-caption">
                            <h1  id="red"><i>A vast collection for you to choose from</i></h1>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img className="d-block w-100" src={pic3} alt="Third slide" />
                        <div className="carousel-caption">
                            <h1  id="red"><i>Find written works not available anywhere!</i></h1>
                        </div>
                    </div>
                </div>
                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>

            <Row className='py-5'>
                <Col xs={12} md={4}>

                    <Card className='cardHighlight p-3'>
                        <Card.Body>
                            <Card.Title>
                                <h2>Textbooks</h2>
                            </Card.Title>
                            <Card.Text>
                            Books containing a comprehensive compilation of content in a branch of study with the intention of explaining it
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                <Col xs={12} md={4}>
                    <Card className='cardHighlight p-3'>
                        <Card.Body>
                            <Card.Title>
                                <h2>Biographies</h2>
                            </Card.Title>
                            <Card.Text>
                                Works literature, commonly considered nonfictional, the subject of which is the life of an individual.
                            </Card.Text>
                        </Card.Body>
                    </Card>

                </Col>

                <Col xs={12} md={4}>
                    <Card className='cardHighlight p-3'>
                        <Card.Body>
                            <Card.Title>
                                <h2>Novels</h2>
                            </Card.Title>
                            <Card.Text>
                            A genre of fiction, and fiction may be defined as the art or craft of contriving, through the written word, representations of human life that instruct or divert or both.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>




        </div>


    )



}