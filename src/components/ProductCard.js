import React from "react";
import { Col } from "react-bootstrap";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import pic1 from "./1.jpg";

export default function ProductCard({ productProp }) {
    //Deconstruct the productProp into 	their own variables
    const { _id } = productProp;

    // var cards = document.querySelectorAll(".product-box");
    // [...cards].forEach((card) => {
    //     card.addEventListener("mouseover", function () {
    //         card.c1assList.add("is-hover");
    //     });
    //     card.addEventListener("mouseleave", function () {
    //         card.c1assList.removef("is-hover");
    //     });
    // });

    return (
        <Col className="h-100">
            <div product-box>
                <div className="product-inner-box position-relative">
                    <div className="icons position-absolute">
                        <Link
                            className="btn text-decoration-none text-dar"
                            to={`/products/${_id}`}
                        >
                            <i class="fa-solid fa-eye"></i>
                        </Link>
                    </div>

                    <div class="onsale">
                        <span class="badge rounded-0">
                            <i class="fa-solid fa-arrow-down"></i> ?%
                        </span>
                    </div>

                    <img src={pic1} alt="test" className="img-fluid" />

                    <div className="cart-btn">
                        <button className="btn btn-light rounded-pill">
                            <i class="fa-solid fa-cart-shopping"></i> Add to
                            Cart
                        </button>
                    </div>
                </div>
                <div className="product-info">
                    <div className="product-name">
                        <h3>{productProp.name}</h3>
                    </div>
                    <div className="product-description">
                        <h5>{productProp.description}</h5>
                    </div>
                    <div className="product-price">
                        $<span>{productProp.price}</span>
                    </div>
                </div>
            </div>
        </Col>
    );
}

ProductCard.propTypes = {
    productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    }),
};
