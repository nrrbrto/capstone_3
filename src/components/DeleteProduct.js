import React, { } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteProd({ product, fetchData }) {

    const deleteProd = (productId) => {
        fetch(`https://ecommerce-csp2.herokuapp.com/products/deleteProd/${productId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
            .then(data => {
                console.log(data)
                    Swal.fire({
                        title: 'Success',
                        icon: 'success',
                        text: 'Product successfully deleted'
                    })
                    fetchData();
				})
    }
    return (
        <>

            <Button variant="danger" size="sm" onClick={() => deleteProd(product)}>Delete</Button>

           
    </>

    )
}