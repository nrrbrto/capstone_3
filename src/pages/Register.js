import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

import image from './background.jpeg';

export default function Register() {

	const { user } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [gender, setGender] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
		if ((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== "" && gender !== "" && mobileNumber !== "") && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, gender, mobileNumber])

	function registerUser(e) {
		e.preventDefault();
		fetch('https://ecommerce-csp2.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password1,
				checkPass: password2,
				firstName: firstName,
				lastName: lastName,
				gender: gender,
				mobileNo: mobileNumber
			})
		}).then( res => res.json())
			.then(result => {
			if (result.message === "Email Successfully Registered"){
				Swal.fire({
					title: "Yes!",
					icon: "success",
					text: "Your Email is Successfully Registered"
				})
				console.log("where the fuck")
			}
			else if (result.message === "Email Already Registered"){
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: "Something Went Wrong. Email Already Been Registered"
				})
			}
				
			
		})
		setEmail('');
		setPassword1('');
		setPassword2('');
		setFirstName('');
		setLastName('');
		setGender("");
		setMobileNumber	('');
	}

	var phoneField = document.getElementById('phoneField');
	if (phoneField) {
		phoneField.addEventListener('keyup', function () {
			var phoneValue = phoneField.value;
			var output;
			phoneValue = phoneValue.replace(/[^0-9]/g, '');
			var area = phoneValue.substr(0, 3);
			var pre = phoneValue.substr(3, 3);
			var tel = phoneValue.substr(6, 4);
			
			if (area.length < 3) {
				output = `${area}`;
			} else if (area.length === 3 && pre.length < 3) {
				output = `(${area}) ${pre}`;
			} else if (area.length === 3 && pre.length === 3) {
				output = `(${area}) ${pre} ${tel}`
			}
			phoneField.value = output;

		});
	}


	return (

		//Conditional rendering
		(user.accessToken !== null) ?

			<Navigate to="/" />

			:
			<div className='vCenterItems'>
				<div className="row">
					<div className="col-md-6 col-lg-6 col-xl-6">


						<Form onSubmit={(e) => registerUser(e)} className='py-3'>

							<Form.Group  >
								<h1>Register</h1>
								<Form.Label>Email Adress</Form.Label>
								<Form.Control
									type="email"
									placeholder="Enter Email"
									required
									value={email}
									onChange={e => setEmail(e.target.value)}
								/>
								<Form.Text className="text-muted">
									We'll never share your email with anyone else
								</Form.Text>
							</Form.Group>


							<div className="row py-3">
								<div className="col">

									<Form.Group>
										<Form.Label>Password</Form.Label>
										<Form.Control
											type="password"
											placeholder="Enter Password"
											required
											value={password1}
											onChange={e => setPassword1(e.target.value)}
										/>
									</Form.Group>
								</div>

								<div className="col">
									<Form.Group>
										<Form.Label>Confirm Password</Form.Label>
										<Form.Control
											type="password"
											placeholder="Confirm Password"
											required
											value={password2}
											onChange={e => setPassword2(e.target.value)}
										/>
									</Form.Group>
								</div>

							</div>

							<div className="row py-3" >
								<div className="col">
									<Form.Group>
										<Form.Label>First Name</Form.Label>
										<Form.Control
											placeholder="Enter First Name"
											required
											value={firstName}
											onChange={e => setFirstName(e.target.value)}
										/>
									</Form.Group>
								</div>
								<div className="col">
									<Form.Group>
										<Form.Label>Last Name</Form.Label>
										<Form.Control
											placeholder="Enter Last Name"
											required
											value={lastName}
											onChange={e => setLastName(e.target.value)}
										/>
									</Form.Group>
								</div>
							</div>

							<div className='py-3'>
								<Form.Group>
									<Form.Label>Gender</Form.Label>
									<div className="row" onChange={e => setGender(e.target.value)} checked={gender}>
										<div className="col">
											<label >
												<input type="radio" value="Male" name='gender'></input>

												Male
											</label>

											<label >
												<input type="radio" value="Female" name='gender'></input>
												Female
											</label>
											<label>
												<input type="radio" value="Other" name='gender'
												/>
												Other
											</label>
										</div>
									</div>

								</Form.Group>

							</div>


							<div className='py-3'>
								<Form.Group >
									<Form.Label>Enter Mobile Number</Form.Label>
									<div className="input-group mb-3">
										<div className="input-group-prepend">
											<span className="input-group-text" id="basic-addon1">+63</span>
										</div>
										<input id="phoneField" type="text" className="form-control" placeholder="(123) 456 7890" value={mobileNumber}
											onChange={e => setMobileNumber(e.target.value)} />
									</div>


								</Form.Group>


								{isActive ?
									<Button variant="primary" type="submit" className="mt-3">Submit</Button>

									:

									<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>

								}


							</div>

						</Form>

					</div>
					<div className="col-md-6 col-lg-6 col-xl-6">
						<img className="d-block w-100 h-100" src={image} alt="side pic" />

					</div>
				</div>



			</div>












	)

}
