import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){
    return(
        <div className='container'>
            <Banner/>
            <Highlights />
        </div>
    )
}