import React, { useContext, useState, useEffect } from 'react';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

import UserContext from '../UserContext';

export default function Products() {

	const { user } = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () => {
		fetch('https://ecommerce-csp2.herokuapp.com/products/all')
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setAllProducts(data)
			})
	}

	useEffect(() => {
		fetchData()
	}, [])

	return (
		 <>
			
					{(user.isAdmin === true) ?

						<AdminView productData={allProducts} fetchData={fetchData} />

						:

						<UserView productData={allProducts} />

					}
		</>

	)
}
