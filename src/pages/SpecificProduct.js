import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function SpecificProduct() {

	const navigate = useNavigate();
	const { productId } = useParams();

	useEffect(() => {

		fetch(`https://ecommerce-csp2.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	} ,[])

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [qty, setQty] = useState('')

	const order = (productId) => {
		fetch('https://ecommerce-csp2.herokuapp.com/orders/order', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId,
				productQty: qty
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: 'Successfully ordered',
					icon: 'success'
				})

				navigate('/products')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		})
	}

	return (
		<Container className="my-3">
			<Card>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price}</h6>
				</Card.Body>

				<Card.Footer>

					{
                        user.accessToken !== null ?
                        <div className="d-grid gap-2">
						<Form.Group>
							<Form.Label>Quantity</Form.Label>
							<Form.Control type="number" value={qty} onChange={e => setQty(e.target.value)} required/>
						</Form.Group>
                            <Button variant="primary" onClick={() => order(productId)}>
                                Order	
                            </Button>
                        </div>
                        :
                        <Link className="btn btn-warning d-grid gap-2" to="/login">
                            Login to Order
                        </Link>
                    }
					
				</Card.Footer>
			</Card>
		</Container>

		)
}


