import React, { useState, useEffect, useContext } from "react"; 
import { Form, Button, Row } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Login() {
    const navigate = useNavigate();
    const { user, setUser } = useContext(UserContext);
    const [email, loginEmail] = useState('');
    const [pass, loginPass] = useState('');
    const [isActive, setIsActive] = useState(true)

    useEffect(() => {
        if (email !== '' && pass !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, pass])

    function authenticate(e) {
        e.preventDefault();

        fetch('https://ecommerce-csp2.herokuapp.com/users/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: pass
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data.accessToken !== undefined) {
                    localStorage.setItem('accessToken', data.accessToken);
                    setUser({
                        accessToken: data.accessToken
                    })
                    
                    Swal.fire({
                        title: "Yes!",
                        icon: "success",
                        text: "You have successfully logged in!"
                    })
                    fetch('https://ecommerce-csp2.herokuapp.com/users/getUserDetails', {
                        headers: {
                            Authorization: `Bearer ${data.accessToken}`
                        }
                    })
                        .then(res => res.json())
                        .then(result => {
                            console.log(result)
                            localStorage.setItem('email', result.email)
                                localStorage.setItem('isAdmin', result.isAdmin)
                                setUser({
                                    email: result.email,
                                    isAdmin: result.isAdmin
                                })
                            if (result.isAdmin === true) {
                                //redirect the admin to /products
                                navigate('/products')
                            } else {
                                //if not admin
                                navigate('/')
                            }
                        })
                } 
                
                if (((data.message === "Wrong Credentials"))){
                    console.log(data)
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: "Something Went Wrong. Check Your Credentials"
                    })
                    loginPass("")
                }

                if ((data.message === "User Not Found")) {
                    Swal.fire({
                                title: 'Oops!',
                                icon: 'error',
                                text: "Something Went Wrong. Check Your Credentials"
                            })
                            loginEmail("")
                    loginPass("")
                }

            })
    }

    return (

        (user.accessToken !== null) ?
            <Navigate to="/products" />
            :

            <Form onSubmit={(e) => authenticate(e)} className='my-4'>
                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter Email"
                        required
                        value={email}
                        onChange={e => loginEmail(e.target.value)}
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter Password"
                        required
                        value={pass}
                        onChange={e => loginPass(e.target.value)}
                    />
                </Form.Group>

                {isActive ?
                    <Row>
                        <Button variant="primary" type="submit" className="mt-3"> Login </Button>
                        <Button variant="primary" type="submit" className="mt-3" onClick={() => navigate("/")}> Cancel </Button>
                    </Row>

                    :

                    <Row>
                        <Button variant="primary" type="submit" className="mt-3" disabled> Login </Button>
                        <Button variant="primary" type="submit" className="mt-3" onClick={() => navigate("/")}> Cancel </Button>
                    </Row>


                }


            </Form>
    )
}
